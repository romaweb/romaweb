import { RouterModule, Routes } from '@angular/router';

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { FooterComponent } from './footer/footer.component';
import { MicuentaComponent } from './micuenta/micuenta.component';
import { ListadedeseosComponent } from './listadedeseos/listadedeseos.component';
import { InicioComponent } from './inicio/inicio.component';
import { LoginComponent } from './login/login.component';
import { MicarritoComponent } from './micarrito/micarrito.component';
import { RComponent } from './r/r.component';
import { PaginaTiendaComponent } from './pagina-tienda/pagina-tienda.component';
import { ContactosComponent } from './contactos/contactos.component';
import { IniciarComponent } from './iniciar/iniciar.component';

const routes: Routes = [
  { path: 'micuenta', component: MicuentaComponent },
  { path: 'listadedeseos', component: ListadedeseosComponent },
  { path: 'micarrito', component: MicarritoComponent },
  { path: 'r', component: RComponent},
  { path: 'r', component: RComponent},
  { path: 'login', component: LoginComponent},
  { path: 'contactos', component: ContactosComponent },
  { path: 'PaginaTienda', component: PaginaTiendaComponent },
  { path: '',component: InicioComponent, pathMatch: 'full' },
  { path: '**',redirectTo: '/', pathMatch: 'full' }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    MicuentaComponent,
    ListadedeseosComponent,
    InicioComponent,
    LoginComponent,
    MicarritoComponent,
    RComponent,
    PaginaTiendaComponent,
    ContactosComponent,
    IniciarComponent

  ],
  imports: [
    BrowserModule,
    
    RouterModule.forRoot(routes)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
