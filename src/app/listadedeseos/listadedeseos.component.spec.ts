import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListadedeseosComponent } from './listadedeseos.component';

describe('ListadedeseosComponent', () => {
  let component: ListadedeseosComponent;
  let fixture: ComponentFixture<ListadedeseosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListadedeseosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListadedeseosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
